<?php
session_start();

  if($_SESSION['id_level']==""){
    header("location:../");
  }

  elseif($_SESSION['id_level']=="2"){
    header("location:../");
  }

  elseif($_SESSION['id_level']=="3"){
    header("location:../	");
  }

//cek apakah user sudah login
if(!isset($_SESSION['status'])){
    die("<script>alert('Login terlebih dahulu!');document.location.href='../index.php'</script>");//
}

?>
<?php
    include "header.php";
     include "koneksi.php";


?>

            <!--===================================================-->
            <!--END NAVBAR-->
            <div class="boxed">
                <!--CONTENT CONTAINER-->
                <!--===================================================-->
                <div id="content-container">

                 <div class="row">      
                 <div class="pageheader">
                 <h3><i class="fa fa-home"></i> Datatable Table </h3>
                    </div> <!--Page content-->
                    <!--===================================================-->
                    <div id="page-content">
                       <!-- Add Row -->
                               <div class="row">
                            <div class="col-lg-3 col-md-3 col-sm-6 col-xm-12">
                                <!--Registered User-->
                                <div class="panel media pad-all">
                                    <div class="media-left">
                                        <span class="icon-wrap icon-wrap-sm icon-circle bg-success">
                                        <i class="fa fa-user fa-2x"></i>
                                        </span>
                                    </div>
                                    <div class="media-body">
                                       <p class="text-2x mar-no text-thin text-right"><?php 
                                            $query = mysqli_num_rows(mysqli_query($koneksi,"SELECT * FROM inventaris"));
                                            echo $query;
                                            ?></p></p>

                                        <p class="h5 mar-no text-right">Inventaris</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-6 col-xm-12">
                                <!--New Order-->
                                <div class="panel media pad-all">
                                    <div class="media-left">
                                        <span class="icon-wrap icon-wrap-sm icon-circle bg-info">
                                        <i class="fa fa-user fa-2x"></i>
                                        </span>
                                    </div>
                                    <div class="media-body">
                                        <p class="text-2x mar-no text-thin text-right"><?php 
                                            $query = mysqli_num_rows(mysqli_query($koneksi,"SELECT * FROM jenis"));
                                            echo $query;
                                            ?></p></p>

                                        <p class="h5 mar-no text-right">Jenis</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-6 col-xm-12">
                                <!--Comments-->
                                <div class="panel media pad-all">
                                    <div class="media-left">
                                        <span class="icon-wrap icon-wrap-sm icon-circle bg-warning">
                                        <i class="fa fa-user fa-2x"></i>
                                        </span>
                                    </div>
                                    <div class="media-body">
                                        <p class="text-2x mar-no text-thin text-right"><?php 
                                            $query = mysqli_num_rows(mysqli_query($koneksi,"SELECT * FROM ruang"));
                                            echo $query;
                                            ?></p></p>
                                        <p class="h5 mar-no text-right">Ruang</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-6 col-xm-12">
                                <!--Sales-->
                                <div class="panel media pad-all">
                                    <div class="media-left">
                                        <span class="icon-wrap icon-wrap-sm icon-circle bg-danger">
                                        <i class="fa fa-user fa-2x"></i>
                                        </span>
                                    </div>
                                    <div class="media-body">
                                        <p class="text-2x mar-no text-thin text-right"><?php 
                                            $query = mysqli_num_rows(mysqli_query($koneksi,"SELECT * FROM detail_pinjam"));
                                            echo $query;
                                            ?></p></p>
                                        <p class="h5 mar-no text-right">Detail Pinjam</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--===================================================-->
                            <div class="panel">
                            <div class="panel-heading">
                            <h3 class="panel-title">Data Barang</h3>
                            </div>
                            <div class="panel-body"> 
                            <div class="row">
                                <div class="col-md-12">
								<div class="table-responsive">
                                    <table id="example" class="table table-striped table-bordered ">
                            <thead>
                            <tr>
        <th>Id Inventaris</th>
        <th>Nama</th>
        <th>Kondisi</th>
        <th>Spesifikasi</th>
        <th>Keterangan</th>
        <th>Jumlah</th>
        <th>Nama Jenis</th>
            <th>Tgl Register</th>
            <th>Nama Ruang</th>
            <th>Kode Inventaris</th>
            <th>Nama Petugas</th>
            <th>Sumber</th>
           
            </tr>
            </thead>
            <tbody>
        <?php
        include 'koneksi.php';
        $no=1;
        $select=mysqli_query($koneksi,"SELECT * FROM inventaris INNER JOIN jenis ON inventaris.id_jenis=jenis.id_jenis INNER JOIN ruang
         ON inventaris.id_ruang=ruang.id_ruang INNER JOIN petugas ON inventaris.id_petugas=petugas.id_petugas ORDER BY inventaris.id_inventaris DESC ");
        while ($data=mysqli_fetch_array($select)) 
        {
        ?>
        <tr>
        <td><?php echo $no++; ?></td>
        <td><?php echo $data['nama']; ?></td>
        <td><?php echo $data['kondisi']; ?></td>
        <td><?php echo $data['spesifikasi']; ?></td>
        <td><?php echo $data['keterangan']; ?></td>
        <td><?php echo $data['jumlah']; ?></td>
        <td><?php echo $data['nama_jenis']; ?></td>
        <td><?php echo $data['tgl_register']; ?></td>
        <td><?php echo $data['nama_ruang']; ?></td>
        <td><?php echo $data['kode_inventaris']; ?></td>
        <td><?php echo $data['nama_petugas']; ?></td>
        <td><?php echo $data['sumber']; ?></td>
        
                                        </tr>
                                       <?php 
        }
        ?>
                                    </tbody>
                                    
                                </table>
								</div>
                                </div>
                            </div>
                            </div>
                        </div>

                        <!--===================================================-->
                        <!-- End Add Row -->

                   
                      
                      
                    </div>
                    
                    <!--===================================================-->
                    <!--End page content-->
                </div>

                <!--===================================================-->
                <!--END CONTENT CONTAINER-->
                <!--MAIN NAVIGATION-->
                <!--===================================================-->
                <?php
                    include "footer.php";
                ?>