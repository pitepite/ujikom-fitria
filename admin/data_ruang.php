<?php
    include "header.php";
?>
            <!--===================================================-->
            <!--END NAVBAR-->
            <div class="boxed">
                <!--CONTENT CONTAINER-->
                <!--===================================================-->
                <div id="content-container">
                    <div id="profilebody">
                        <div class="pad-all animated fadeInDown">
                            <div class="row">
                                
                                
                               
                               
                               
                                
                            </div>
                        </div>
                    </div>
                    <div class="pageheader">
                        <h3><i class="fa fa-home"></i> Datatable Table </h3>
                       
                    </div>
                    <!--Page content-->
                    <!--===================================================-->
                    <div id="page-content">
                       <!-- Add Row -->
                        <!--===================================================-->

                        <!--===================================================-->
                        <!-- End Add Row -->

                   
                      
                      
                    </div>
					     <div id="page-content">
                       <!-- Add Row -->
                        <!--===================================================-->
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title">Data Ruang</h3>
                            </div>
                            <div class="panel-body">
                           <a href="tambah_ruang.php">
                                <button class="btn btn-pink">+ Tambah Ruang</button></a>
                            
                                <table id="example" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
         <th>No</th>
        <th>Id Ruang</th>
        <th>Nama Ruang</th>
        <th>Kode Ruang</th>
        <th>Keterangan</th>
            <th>Aksi</th>         </thead>
                                    <tbody>
      <?php
        include 'koneksi.php';
        $no=1;
        $select=mysqli_query($koneksi,"select * from ruang order by id_ruang desc");
        while ($data=mysqli_fetch_array($select)) 
        {
        ?>
         <tr>
       <td><?php echo $no++; ?></td>
        <td><?php echo $data['id_ruang']; ?></td>
        <td><?php echo $data['nama_ruang']; ?></td>
        <td><?php echo $data['kode_ruang']; ?></td>
        <td><?php echo $data['keterangan']; ?></td>
          <td><a class="btn btn outline btn-primary" href="edit_ruang.php?id_ruang=<?php echo
        $data['id_ruang']; ?>" >Edit</a>
        <a class="btn btn outline btn-danger" href="hapus_ruang.php?id_ruang=<?php echo $data['id_ruang'];
        ?>">Hapus</a></td>            </tr>
                                       
                                    
                                    <?php 
        }
        ?>
        </tbody>
                                </table>
                            </div>
                        </div>
                        <!--===================================================-->
                        <!-- End Add Row -->

                   
                      
                      
                    </div>
					
                    <!--===================================================-->
                    <!--End page content-->
                </div>
                <!--===================================================-->
                <!--END CONTENT CONTAINER-->
                <!--MAIN NAVIGATION-->
                <!--===================================================-->
                <?php
                    include "footer.php";
                ?>