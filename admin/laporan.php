<?php
session_start();

//cek apakah user sudah login
if(!isset($_SESSION['status'])){
    die("<script>alert('Login terlebih dahulu!');document.location.href='../index.php'</script>");//
}

?>

<?php
    include "header.php";
?>
            <!--===================================================-->
            <!--END NAVBAR-->
            <div class="boxed">
                <!--CONTENT CONTAINER-->
                <!--===================================================-->
                <div id="content-container"> 
                            <div class="row">
                                
                                
                               
                               
                               
                                
                    <div class="pageheader">
                        <h3><i class="fa fa-home"></i> Datatable Table </h3>
                       
                   
                    </div> <!--Page content-->
                    <!--===================================================-->
                    <div id="page-content">
                       <!-- Add Row -->
                        <!--===================================================-->
                            <div class="panel">
                            <div class="panel-heading">
                            <h3 class="panel-title">Data Barang</h3>
                            </div>
                            <div  style="margin-left: 30px; margin-top:10px;" data-toggle="modal" data-target="#excel" class="table-toolbar-left">
                            <button  class="btn btn-pink">Rekap Data</button>
                            </div>
                            <div  style="margin-left: 30px; margin-top:10px;" data-toggle="modal" data-target="#export" class="table-toolbar-left">
                            <button  class="btn btn-pink">Cetak PDF</button>
                            </div>
                            <div class="panel-body"> 
                            <div class="row">
                                <div class="col-md-12">
								<div class="table-responsive">
                                    <table id="example" class="table table-striped table-bordered ">
                            <thead>
                            <tr>
        <th>Id Inventaris</th>
        <th>Nama</th>
        <th>Kondisi</th>
        <th>Spesifikasi</th>
        <th>Keterangan</th>
        <th>Jumlah</th>
        <th>Nama Jenis</th>
            <th>Tgl Register</th>
            <th>Nama Ruang</th>
            <th>Kode Inventaris</th>
            <th>Nama Petugas</th>
            <th>Sumber</th>
            </tr>
            </thead>
            <tbody>
        <?php
        include 'koneksi.php';
        $no=1;
        $select=mysqli_query($koneksi,"SELECT * FROM inventaris INNER JOIN jenis ON inventaris.id_jenis=jenis.id_jenis INNER JOIN ruang
         ON inventaris.id_ruang=ruang.id_ruang INNER JOIN petugas ON inventaris.id_petugas=petugas.id_petugas ORDER BY inventaris.id_inventaris DESC ");
        while ($data=mysqli_fetch_array($select)) 
        {
        ?>
        <tr>
        <td><?php echo $no++; ?></td>
        <td><?php echo $data['nama']; ?></td>
        <td><?php echo $data['kondisi']; ?></td>
        <td><?php echo $data['spesifikasi']; ?></td>
        <td><?php echo $data['keterangan']; ?></td>
        <td><?php echo $data['jumlah']; ?></td>
        <td><?php echo $data['nama_jenis']; ?></td>
        <td><?php echo $data['tgl_register']; ?></td>
        <td><?php echo $data['nama_ruang']; ?></td>
        <td><?php echo $data['kode_inventaris']; ?></td>
        <td><?php echo $data['nama_petugas']; ?></td>
        <td><?php echo $data['sumber']; ?></td>
        
                                        </tr>
                                       <?php 
        }
        ?>
                                    </tbody>
                                    
                                </table>
                                <div id="excel" class="modal fade" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title">Export Data Barang </h4>
</div>
<div class="modal-body">
<form action="export_per.php" method="post" target="_blank">
<table>
    <tr>
        <td>
            <div class="form-group">Dari Tanggal</div>
        </td>
        <td align="center" width="5%">
            <div class="form-group">:</div>
        </td>
        <td>
           <div class="form-group">
               <input type="date" class="form-control" name="tgl_a" required>
           </div> 
        </td>
    </tr>
    <tr>
        <td>
            <div class="form-group">Sampai Tanggal</div>
        </td>
        <td align="center">
            <div class="form-group">:</div>
        </td>
        <td>
           <div class="form-group">
               <input type="date" class="form-control" name="tgl_b" required>
           </div> 
        </td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td>
            <input type="submit" name="export_barang" class="btn btn-info btn-sm" value="Export">
        </td>
    </tr>
</table>
</form>
</div>
<div class="modal-footer">
<a href="export_excel_barang.php"  class="btn btn-info btn-sm" >Export Semua</a>
</div>
</div>
</div>
</div>
</div>
						

<div id="export" class="modal fade" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title">Export Data Barang </h4>
</div>
<div class="modal-body">
<form action="cetak_tgl.php" method="post" target="_blank">
<table>
    <tr>
        <td>
            <div class="form-group">Dari Tanggal</div>
        </td>
        <td align="center" width="5%">
            <div class="form-group">:</div>
        </td>
        <td>
           <div class="form-group">
               <input type="date" class="form-control" name="tgl_a" required>
           </div> 
        </td>
    </tr>
    <tr>
        <td>
            <div class="form-group">Sampai Tanggal</div>
        </td>
        <td align="center">
            <div class="form-group">:</div>
        </td>
        <td>
           <div class="form-group">
               <input type="date" class="form-control" name="tgl_b" required>
           </div> 
        </td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td>
            <input type="submit" name="export_barang" class="btn btn-info btn-sm" value="Export">
        </td>
    </tr>
</table>
</form>
</div>
<div class="modal-footer">
<a href="cetak_inventaris.php"  class="btn btn-info btn-sm" >Export Semua</a>
</div>
</div>
</div>
</div>
</div>
                        		</div>
                                </div>
                            </div>
                            </div>
                        </div>

                        <!--===================================================-->
                        <!-- End Add Row -->

                   
                      
                      
                    </div>
                    
                    <!--===================================================-->
                    <!--End page content-->
                </div>

                <!--===================================================-->
                <!--END CONTENT CONTAINER-->
                <!--MAIN NAVIGATION-->
                <!--===================================================-->
                <?php
                    include "footer.php";
                ?>