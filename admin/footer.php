 <nav id="mainnav-container">
                    <div id="mainnav">
                        <!--Menu-->
                        <!--================================-->
                        <div id="mainnav-menu-wrap">
                            <div class="nano">
                                <div class="nano-content">
                                    <ul id="mainnav-menu" class="list-group">
                                        <!--Category name-->
                                        <li class="list-header">Admin</li>
										  <span class =""><?=date("l d/m/Y")?></span> </p>
                                        <!--Menu list item-->
                                        <li>
                                             <a href="#">
                                                <i class="fa fa-home"></i>
                                        <span class="menu-title">Dashboard</span>
                                            <i class="arrow"></i>
                                            </a>
                                            <!--Submenu-->
                                            <ul class="collapse">
                                                <li><a href="dashboard.php"><i class="fa fa-caret-right"></i>Inventaris</a></li>
                                                <li><a href="#"><i class="fa fa-caret-right"></i> D2</a></li>
                                                <li><a href="#"><i class="fa fa-caret-right"></i> D3</a></li>
           
                                            </ul>
                                            
                                        <!--Menu list item-->
                                        <li>
                                            <a href="#">
                                                <i class="fa fa-home"></i>
                                        <span class="menu-title">Inventaris</span>
                                            <i class="arrow"></i>
                                            </a>
                                            <!--Submenu-->
                                            <ul class="collapse">
                                                <li><a href="data_barang.php"><i class="fa fa-caret-right"></i> Data Barang</a></li>
                                                <li><a href="data_jenis.php"><i class="fa fa-caret-right"></i> Data Jenis</a></li>
                                                <li><a href="data_ruang.php"><i class="fa fa-caret-right"></i> Data Ruang</a></li>
           
                                            </ul>
                                        </li>
                                                <li>
                                            <a href="javascript:void(0)">
                                            <i class="fa fa-users"></i>
                                            <span class="menu-title">Pegawai</span>
                                            <i class="arrow"></i>
                                            </a>
                                            <!--Submenu-->
                                            <ul class="collapse">
                                                <li><a href="data_pegawai.php"><i class="fa fa-caret-right"></i> Data Pegawai</a></li>
                                            </ul>
                                        </li>
                                                 <li>
                                            <a href="javascript:void(0)">
                                            <i class="fa fa-user"></i>
                                            <span class="menu-title">Petugas</span>
                                            <i class="arrow"></i>
                                            </a>
                                            <!--Submenu-->
                                            <ul class="collapse">
                                                <li><a href="data_petugas.php"><i class="fa fa-caret-right"></i> Data Petugas</a></li>
                                            </ul>
                                        </li>
										<li>
                                            <a href="#"> 
                                                <i class="fa fa-exchange"></i>
                                            <span class="menu-title">Transaksi</span>
                                            <i class="arrow"></i>
                                            </a>
                                            <!--Submenu-->
                                            <ul class="collapse">
                                                <li><a href="peminjaman.php"><i class="fa fa-caret-right"></i> Peminjaman</a></li>
                                            
                                                <li><a href="data_peminjam.php"><i class="fa fa-caret-right"></i>Data Peminjaman</a></li>
                                                
                                            </ul>
                                        </li>
                                        <li>
                                            <a href="backup.php"> <i class="fa fa-cloud-download"></i>
                                        <span class="menu-title">Backup</span>
                                    </a>
                                </li>

                                <li>
                                            <a href="javascript:void(0)">
                                            <i class="fa fa-print"></i>
                                            <span class="menu-title">Laporan</span>
                                            <i class="arrow"></i>
                                            </a>
                                            <!--Submenu-->
                                            <ul class="collapse">
                                                <li><a href="laporan.php"><i class="fa fa-caret-right"></i> Laporan Barang</a></li>
                                                <li><a href="laporan_peminjaman.php"><i class="fa fa-caret-right"></i> Laporan Peminjaman</a></li>
                                            </ul>
                                        </li>
           
                                </div>
                            </div>
                        </div>
                    </div>
                        <!--================================-->
                        <!--End menu-->
                   
                </nav>
               
            
            <!-- FOOTER -->
            <!--===================================================-->
            <footer id="footer">
                <!-- Visible when footer positions are fixed -->
                <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
                <div class="show-fixed pull-right">
                    <ul class="footer-list list-inline">
                        <li>
                            <p class="text-sm">SEO Proggres</p>
                            <div class="progress progress-sm progress-light-base">
                                <div style="width: 80%" class="progress-bar progress-bar-danger"></div>
                            </div>
                        </li>
                        <li>
                            <p class="text-sm">Online Tutorial</p>
                            <div class="progress progress-sm progress-light-base">
                                <div style="width: 80%" class="progress-bar progress-bar-primary"></div>
                            </div>
                        </li>
                        <li>
                            <button class="btn btn-sm btn-dark btn-active-success">Checkout</button>
                        </li>
                    </ul>
                </div>
                <!-- Visible when footer positions are static -->
                <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
                <div class="hide-fixed pull-right pad-rgt">Fitria NP</div>
                <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
                <!-- Remove the class name "show-fixed" and "hide-fixed" to make the content always appears. -->
                <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
                <p class="pad-lft"> @ 2019 SMKN 1 CIOMAS</p>
            </footer>
            <!--===================================================-->
            <!-- END FOOTER -->
            <!-- SCROLL TOP BUTTON -->
            <!--===================================================-->
            <button id="scroll-top" class="btn"><i class="fa fa-chevron-up"></i></button>
            <!--===================================================-->
        
        <!--===================================================-->
        <!-- END OF CONTAINER -->
        <!--JAVASCRIPT-->
        <!--=================================================-->
        <!--jQuery [ REQUIRED ]-->
        <script src="js/jquery-2.1.1.min.js"></script>
        <!--BootstrapJS [ RECOMMENDED ]-->
        <script src="js/bootstrap.min.js"></script>
        <!--Fast Click [ OPTIONAL ]-->
        <script src="plugins/fast-click/fastclick.min.js"></script>
        <!--Jasmine Admin [ RECOMMENDED ]-->
        <script src="js/scripts.js"></script>
        <!--Switchery [ OPTIONAL ]-->
        <script src="plugins/switchery/switchery.min.js"></script>
        <!--Bootstrap Select [ OPTIONAL ]-->
        <script src="plugins/bootstrap-select/bootstrap-select.min.js"></script>
        <!--DataTables [ OPTIONAL ]-->
        <script src="plugins/datatables/media/js/jquery.dataTables.js"></script>
        <script src="plugins/datatables/media/js/dataTables.bootstrap.js"></script>
        <script src="plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js"></script>
        <!--Fullscreen jQuery [ OPTIONAL ]-->
        <script src="plugins/screenfull/screenfull.js"></script>
        <!--Demo script [ DEMONSTRATION ]-->
        <script src="js/demo/jasmine.js"></script>
        <!--DataTables Sample [ SAMPLE ]-->
        <script src="js/demo/tables-datatables.js"></script>
        <script src="js/demo/tables-datatables-1.js"></script>

        <script type="text/javascript">
                $(function() {
                    $('#example').DataTable({
                        'lengthChange':true
                    })
                });
        </script>
    </body>
</html>