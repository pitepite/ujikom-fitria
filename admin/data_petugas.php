<?php
    include "header.php";
?>
            <!--===================================================-->
            <!--END NAVBAR-->
            <div class="boxed">
                <!--CONTENT CONTAINER-->
                <!--===================================================-->
                <div id="content-container"> 
                            <div class="row">
                                
                                
                               
                               
                               
                                
                    <div class="pageheader">
                        <h3><i class="fa fa-home"></i> Datatable Table </h3>
                       
                   
                    </div> <!--Page content-->
                    <!--===================================================-->
                    <div id="page-content">
                       <!-- Add Row -->
                        <!--===================================================-->
                 <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title">Data Petugas</h3>
                            </div>
                            <div id="demo-custom-toolbar2" class="table-toolbar-left"><a href="tambah_petugas.php">
                                <button  class="btn btn-pink">+ Tambah Petugas</button></a>
                            </div>
                            <div class="panel-body"> 
                                <table id="example" class="table table-striped table-bordered table-responsive">
                                    <thead>
                                        <tr>
         <th>No</th>                                   
        <th>Username</th>
        <th>Email</th>
        <th>Password</th>
        <th>Nama Petugas</th>
        <th>Nama Level</th>
        <th>Aksi</th>
        </tr>
        </thead>
        <tbody>

        <?php
        include 'koneksi.php';
        $no=1;
        $select=mysqli_query($koneksi, "SELECT * from petugas INNER JOIN level ON petugas.id_level=level.id_level order by id_petugas desc ");
        while ($data=mysqli_fetch_array($select)) 
        {
        ?>
                                        <tr>
        <td><?php echo $no++; ?></td>
        <td><?php echo $data['username']; ?></td>
        <td><?php echo $data['email']; ?></td>
        <td><?php echo $data['password']; ?></td>
        <td><?php echo $data['nama_petugas']; ?></td>
        <td><?php echo $data['nama_level']; ?></td>
        <td><a class="btn btn outline btn-primary btn-sm" href="edit_petugas.php?id_petugas=<?php echo
        $data['id_petugas']; ?>" >Edit</a>
        <a class="btn btn outline btn-danger btn-sm" href="hapus_petugas.php?id_petugas=<?php echo $data['id_petugas'];
        ?>">Hapus</a></td>
                                        </tr>
                                       <?php 
        }
        ?>
                                    </tbody>
                                    
                                </table>
                            </div>
                        </div>

                        <!--===================================================-->
                        <!-- End Add Row -->

                   
                      
                      
                    </div>
                    
                    <!--===================================================-->
                    <!--End page content-->
                </div>

                <!--===================================================-->
                <!--END CONTENT CONTAINER-->
                <!--MAIN NAVIGATION-->
                <!--===================================================-->
                <?php
                    include "footer.php";
                ?>