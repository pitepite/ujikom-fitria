<?php 
session_start();

?>
<?php
    include "header.php";
?>
            <!--===================================================-->
            <!--END NAVBAR-->
            <div class="boxed">
                <!--CONTENT CONTAINER-->
                <!--===================================================-->
                <div id="content-container"> 
                            <div class="row">
                                
                                
                               
                               
                               
                                
                    <div class="pageheader">
                        <h3><i class="fa fa-home"></i> Datatable Table </h3>
                       
                   
                    </div> <!--Page content-->
                    <!--===================================================-->
                    <div id="page-content">
                       <!-- Add Row -->
                        <!--===================================================-->
                            <div class="panel">
                            <div class="panel-heading">
                            <h3 class="panel-title">Data Barang</h3>
                            </div>
                            <div  style="margin-left: 30px; margin-top:10px;"  id="demo-custom-toolbar" class="table-toolbar-left"><a href="tambah_barang.php">
                            <button  class="btn btn-pink">+ Tambah Data</button></a>
                            </div>
						    <div  style="margin-left: 30px; margin-top:10px;" id="demo-custom-toolbar1" class="table-toolbar-left"><a href="export_excel_barang.php">
                            <button  class="btn btn-pink">Export Excel</button></a>
                            </div>
						

                            <div class="panel-body"> 
                            <div class="row">
                                <div class="col-md-12">
								<div class="table-responsive">
                                    <table id="example" class="table table-striped table-bordered ">
                            <thead>
                            <tr>
        <th>Id Inventaris</th>
        <th>Nama</th>
        <th>Kondisi</th>
        <th>Spesifikasi</th>
        <th>Keterangan</th>
        <th>Jumlah</th>
        <th>Nama Jenis</th>
            <th>Tgl Register</th>
            <th>Nama Ruang</th>
            <th>Kode Inventaris</th>
            <th>Nama Petugas</th>
            <th>Sumber</th>
            <th>Aksi</th>
            </tr>
            </thead>
            <tbody>
        <?php
        include 'koneksi.php';
        $no=1;
        $select=mysqli_query($koneksi,"SELECT * FROM inventaris INNER JOIN jenis ON inventaris.id_jenis=jenis.id_jenis INNER JOIN ruang
         ON inventaris.id_ruang=ruang.id_ruang INNER JOIN petugas ON inventaris.id_petugas=petugas.id_petugas ORDER BY inventaris.id_inventaris DESC ");
        while ($data=mysqli_fetch_array($select)) 
        {
        ?>
        <tr>
        <td><?php echo $no++; ?></td>
        <td><?php echo $data['nama']; ?></td>
        <td><?php echo $data['kondisi']; ?></td>
        <td><?php echo $data['spesifikasi']; ?></td>
        <td><?php echo $data['keterangan']; ?></td>
        <td><?php echo $data['jumlah']; ?></td>
        <td><?php echo $data['nama_jenis']; ?></td>
        <td><?php echo $data['tgl_register']; ?></td>
        <td><?php echo $data['nama_ruang']; ?></td>
        <td><?php echo $data['kode_inventaris']; ?></td>
        <td><?php echo $data['nama_petugas']; ?></td>
        <td><?php echo $data['sumber']; ?></td>
        <td><a class="btn btn outline btn-primary btn-sm" href="edit_barang.php?id_inventaris=<?php echo
        $data['id_inventaris']; ?>" >Edit</a>
        <a class="btn btn outline btn-danger btn-sm" href="hapus_barang.php?id_inventaris=<?php echo $data['id_inventaris'];
        ?>">Hapus</a></td>
                                        </tr>
                                       <?php 
        }
        ?>
                                    </tbody>
                                    
                                </table>
								</div>
                                </div>
                            </div>
                            </div>
                        </div>

                        <!--===================================================-->
                        <!-- End Add Row -->

                   
                      
                      
                    </div>
                    
                    <!--===================================================-->
                    <!--End page content-->
                </div>

                <!--===================================================-->
                <!--END CONTENT CONTAINER-->
                <!--MAIN NAVIGATION-->
                <!--===================================================-->
                <?php
                    include "footer.php";
                ?>