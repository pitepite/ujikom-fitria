<?php
    include "header.php";
?>
            <!--===================================================-->
            <!--END NAVBAR-->
            <div class="boxed">
                <!--CONTENT CONTAINER-->
                <!--===================================================-->
                <div id="content-container"> 
                            <div class="row">
                                
                                
                               
                               
                               
                                
                    <div class="pageheader">
                        <h3><i class="fa fa-home"></i> Datatable Table </h3>
                       
                   
                    </div> <!--Page content-->
                    <!--===================================================-->
                    <div id="page-content">
                       <!-- Add Row -->
                        <!--===================================================-->
                 <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title">Data Pegawai</h3>
                            </div>
                            <div id="demo-custom-toolbar2" class="table-toolbar-left"><a href="tambah_pegawai.php">
                                <button  class="btn btn-pink">+ Tambah Pegawai</button></a>
                            </div>
                            <div class="panel-body"> 
                                <table id="example" class="table table-striped table-bordered table-responsive">
                                    <thead>
                                        <tr>
         <th>No</th>                                   
        <th>Nama</th>
        <th>NIP</th>
        <th>Alamat</th>
        <th>Aksi</th>
        </tr>
        </thead>
        <tbody>

        <?php
        include 'koneksi.php';
        $no=1;
        $select=mysqli_query($koneksi,"select * from pegawai order by id_pegawai desc ");
        while ($data=mysqli_fetch_array($select)) 
        {
        ?>
                                        <tr>
        <td><?php echo $no++; ?></td>
        <td><?php echo $data['nama_pegawai']; ?></td>
        <td><?php echo $data['nip']; ?></td>
        <td><?php echo $data['alamat']; ?></td>
        <td><a class="btn btn outline btn-primary btn-sm" href="edit_pegawai.php?id_pegawai=<?php echo
        $data['id_pegawai']; ?>" >Edit</a>
        <a class="btn btn outline btn-danger btn-sm" href="hapus_pegawai.php?id_pegawai=<?php echo $data['id_pegawai'];
        ?>">Hapus</a></td>
                                        </tr>
                                       <?php 
        }
        ?>
                                    </tbody>
                                    
                                </table>
                            </div>
                        </div>

                        <!--===================================================-->
                        <!-- End Add Row -->

                   
                      
                      
                    </div>
                    
                    <!--===================================================-->
                    <!--End page content-->
                </div>

                <!--===================================================-->
                <!--END CONTENT CONTAINER-->
                <!--MAIN NAVIGATION-->
                <!--===================================================-->
                <?php
                    include "footer.php";
                ?>