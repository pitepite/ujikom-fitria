<?php
    include "header.php";
?>

            <div class="boxed">
                <!--CONTENT CONTAINER-->
                <!--===================================================-->
                <div id="content-container">
                    <div id="profilebody">
                        <div class="pad-all animated fadeInDown">
                            <div class="row">
                                
                                
                               
                               
                               
                                
                            </div>
                        </div>
                    </div>
                    <div class="pageheader">
                        <h3><i class="fa fa-home"></i> Datatable Table </h3>
                       
                    </div>
                    <!--Page content-->
                    <!--===================================================-->
                    <div id="page-content">
                       <!-- Add Row -->
                      <div class="panel">
                                    <!-- Panel heading -->
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Tambah Barang</h3>
                                    </div>
                                      <form action="simpan_barang.php" method="post" class="form-horizontal">
                                        <div class="panel-body">
                                              
                    <div class="form-group">
                        <label>Nama Barang</label>
                        <input type="text" name="nama" class="form-control" autocomplete="off" required>
                    </div>
                
                    <div class="form-group">
                        <label>Kondisi</label>
                        <input type="text"  name="kondisi" class="form-control" autocomplete="off" required>
                    </div>

                     <div class="form-group">
                        <label>Spesifikasi</label>
                        <input type="text" name="spesifikasi" class="form-control" autocomplete="off" required >
                    </div>
                    
                    <div class="form-group">
                        <label>Keterangan</label>
                        <input type="text" name="keterangan" class="form-control" autocomplete="off" required >
                    </div>

                    <div class="form-group">
                        <label>Jumlah</label>
                        <input type="text" name="jumlah" class="form-control" autocomplete="off" required >
                    </div>

                     <div class="form-group">
                        <?php
        include "koneksi.php";
        $result = mysqli_query($koneksi,"select * from jenis order by id_jenis asc ");
        $jsArray = "var id_jenis = new Array();\n";
        ?>

                        <label>Id Jenis</label>
                       
                    <select class="form-control m-bot15" name="id_jenis" onchange="changeValue(this.value)">
                        <option selected="selected">.........pilih jenis.......
                            <?php
                            while($row = mysqli_fetch_array($result)){
                                echo "<option value='$row[0].$row[1]'>$row[0]. $row[1]</option>";
                                $jsArray .= "id_jenis['".$row['id_jenis']."'] = {satu:'" . addslashes($row['no']) . "'};\n";
                            }
                            ?>
                        </option>
                            </select>
                        </div>


                    <div class="form-group">
                        <label>Tgl Register</label>
                         <input type="date" value="<?php $tgl=Date('Y-m-d'); echo $tgl;?>" required="" name="tgl_register" class="form-control"  >
                    </div>

                   <div class="form-group">
                        <?php
        include "koneksi.php";
        $result = mysqli_query($koneksi,"select * from ruang order by id_ruang asc ");
        $jsArray = "var id_ruang = new Array();\n";
        ?>

                        <label>Id Ruang</label>
                       
                    

                    <select class="form-control m-bot15" name="id_ruang" onchange="changeValue(this.value)">
                        <option selected="selected">.........pilih Ruang.......
                            <?php
                            while($row = mysqli_fetch_array($result)){
                                echo "<option value='$row[0].$row[1]'>$row[0]. $row[1]</option>";
                                $jsArray .= "id_ruang['".$row['id_ruang']."'] = {satu:'" . addslashes($row['no']) . "'};\n";
                            }
                            ?>
                        </option>
                            </select>
                        </div>

                 <?php
            $koneksi = mysqli_connect("localhost","root","","inventaris");

            $cari_kd=mysqli_query($koneksi,"select max(kode_inventaris)as kode from inventaris");
            //mencari kode yang paling besar atau kode yang baru masuk
            $tm_cari=mysqli_fetch_array($cari_kd);
            $kode=substr($tm_cari['kode'],1,4);
            //mengambil string mulai dari karakter pertama 'A' dan mengambil karakter setelah nya
            $tambah=$kode+1;
            //kode yang sudah dipecah di tambah 1
            if($tambah<10){//jika kode lebih kecil dari 10(0,8,7,6, dst) maka
                $kode_inventaris="8000" .$tambah;
            } else{
                $kode_inventaris="800" .$tambah;
            }
            ?>


                    <div class="form-group">
                        <label>Kode Inventaris</label>
                        <input type="text" name="kode_inventaris" value="<?php echo $kode_inventaris;?>" class="form-control" required="" readonly >
                    </div>


                    <div class="form-group">
                        <?php
        include "koneksi.php";
        $result = mysqli_query($koneksi,"select * from petugas order by id_petugas asc ");
        $jsArray = "var id_petugas = new Array();\n";
        ?>

                        <label>Id Petugas</label>
                       
                    

                    <select class="form-control m-bot15" name="id_petugas" onchange="changeValue(this.value)">
                        <option selected="selected">.........pilih Petugas.......
                            <?php
                            while($row = mysqli_fetch_array($result)){
                                echo "<option value='$row[0].$row[1]'>$row[0]. $row[1]</option>";
                                $jsArray .= "id_petugas['".$row['id_petugas']."'] = {satu:'" . addslashes($row['no']) . "'};\n";
                            }
                            ?>
                        </option>
                            </select>
							</div>

                             <div class="form-group">
                        <label>Sumber</label>
                        <input type="text" name="sumber" class="form-control" autocomplete="off" required >
                    </div>
                        
                    <button type="submit" >Simpan</button>
                </form>                                 
                                            <!--===================================================-->
                                        
                                        
                                
                                
                        <!--===================================================-->
                        <!-- End Add Row -->

                   
                      
                      
                    </div>
                    <!--===================================================-->
                    <!--End page content-->
                </div>
				</div>
                <!--===================================================-->
                <!--END CONTENT CONTAINER-->
                <!--MAIN NAVIGATION-->
                <!--===================================================-->
               
<?php
    include "footer.php";
?>