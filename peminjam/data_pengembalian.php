<?php 
session_start();

?>
<?php
    include "header.php";
?>
            <!--===================================================-->
            <!--END NAVBAR-->
            <div class="boxed">
                <!--CONTENT CONTAINER-->
                <!--===================================================-->
                <div id="content-container"> 
                            <div class="row">
                                
                                
                               
                               
                               
                                
                    <div class="pageheader">
                        <h3><i class="fa fa-home"></i> Datatable Table </h3>
                       
                   
                    </div> <!--Page content-->
                    <!--===================================================-->
                    <div id="page-content">
                       <!-- Add Row -->
                        <!--===================================================-->
                            <div class="panel">
                            <div class="panel-heading">
                            <h3 class="panel-title">Data Pengembalian</h3>
                            </div>
                           
						    <div  style="margin-left: 30px; margin-top:10px;" id="demo-custom-toolbar1" class="table-toolbar-left"><a href="export_excel_barang.php">
                            <button  class="btn btn-pink">Export Excel</button></a>
                            </div>
						

                            <div class="panel-body"> 
                            <div class="row">
                                <div class="col-md-12">
								<div class="table-responsive">
                                    <table id="example" class="table table-striped table-bordered ">
                            <thead>
                            <tr>
        <th>Id Peminjaman</th>
        <th>Tanggal Pinjam</th>
        <th>Tanggal Kembali</th>
        <th>Status Peminjaman</th>
        <th>Nama Peminjam</th>
        <th>Detail</th>
            </tr>
            </thead>
            <tbody>
        <?php
        include 'koneksi.php';
        $no=1;
        $select=mysqli_query($koneksi,"SELECT * FROM peminjaman INNER JOIN pegawai ON peminjaman.id_pegawai=pegawai.id_pegawai ORDER BY id_peminjaman DESC ");
        while ($data=mysqli_fetch_array($select)) 
        {
        ?>
        <tr>
        <td><?php echo $no++; ?></td>
        <td><?php echo $data['tgl_pinjam']; ?></td>
        <td><?php echo $data['tgl_kembali']; ?></td>
        <td><?php echo $data['status_peminjaman']; ?></td>
        <td><?php echo $data['nama_pegawai']; ?></td>
         <td><a class="btn btn outline btn-primary btn-sm" href="edit_petugas.php?id_petugas=<?php echo
        $data['id_petugas']; ?>" >Detail</a>

       
                                        </tr>
                                       <?php 
        }
        ?>
                                    </tbody>
                                    
                                </table>
								</div>
                                </div>
                            </div>
                            </div>
                        </div>

                        <!--===================================================-->
                        <!-- End Add Row -->

                   
                      
                      
                    </div>
                    
                    <!--===================================================-->
                    <!--End page content-->
                </div>

                <!--===================================================-->
                <!--END CONTENT CONTAINER-->
                <!--MAIN NAVIGATION-->
                <!--===================================================-->
                <?php
                    include "footer.php";
                ?>